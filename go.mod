module crudapp

go 1.15

require (
	github.com/cockroachdb/apd v1.1.0 // indirect
	github.com/gofrs/uuid v4.0.0+incompatible // indirect
	github.com/golang/mock v1.6.0
	github.com/gorilla/mux v1.8.0
	github.com/jackc/fake v0.0.0-20150926172116-812a484cc733 // indirect
	github.com/jackc/pgx v3.6.2+incompatible
	github.com/lib/pq v1.10.2 // indirect
	github.com/shopspring/decimal v1.2.0 // indirect
	github.com/speps/go-hashids/v2 v2.0.1
	github.com/spf13/viper v1.8.1
	github.com/stretchr/testify v1.7.0
	github.com/yeqown/go-qrcode v1.5.7
	go.uber.org/zap v1.18.1
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9
	gopkg.in/DATA-DOG/go-sqlmock.v1 v1.3.0
)
