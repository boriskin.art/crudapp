package paste

import "crudapp/internal/pkg/models"

//go:generate mockgen -destination=./mock/mock_repo.go -package=mock crudapp/internal/pkg/paste Repository

type Repository interface {
	GetByToken(token string) (*models.Paste, error)
	GetByUserID(userID int) ([]models.Paste, error)
	Create(p *models.Paste) (int64, error)
	GetPastesCount() (int, error)
}