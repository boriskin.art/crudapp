package delivery

import (
	"crudapp/internal/crudapp/tempate"
	"crudapp/internal/pkg/models"
	"crudapp/internal/pkg/paste"
	"crudapp/pkg/contextData"
	"crudapp/pkg/token"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/spf13/viper"
	"go.uber.org/zap"
	"html/template"
	"net/http"
	"os"
	"time"
)

type PasteHandler struct {
	Logger 		*zap.SugaredLogger
	PasteRepo 	paste.Repository
	Tmpl 		*template.Template
}

var files = []string{
	"./static/templates/pasteit.page.tmpl",
	"./static/templates/base.layout.tmpl",
}

func (h *PasteHandler) Open(w http.ResponseWriter, r *http.Request) {
	token := mux.Vars(r)["token"]

	p, err := h.PasteRepo.GetByToken(token)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	QRImg, err := models.GenerateQRCode(viper.GetString("domain") + "/pasteit/" + p.Token, p.Token)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	fmt.Println("Paste:", QRImg)

	data := contextData.GetDataFromContext(r.Context())
	data["pasteName"] = p.Name
	data["pasteText"] = p.Text
	data["QRImg"] = QRImg


	go func(QRImg string) {
		time.Sleep(5 * time.Minute)
		os.Remove(QRImg)
	}(QRImg)

	data["Paste"] = p.Name

	r.WithContext(contextData.SaveDataToContext(data))

	err = tempate.ExecuteTemplate(r.Context(), w, files)
	if err != nil {
		h.Logger.Error("Template error", err)
		http.Error(w, "Template error", http.StatusInternalServerError)
		return
	}
}

func (h *PasteHandler) Pastes(w http.ResponseWriter, r *http.Request) {
	data := contextData.GetDataFromContext(r.Context())
	userID := data["userID"].(int)

	pastes, err := h.PasteRepo.GetByUserID(userID)
	if err != nil {
		h.Logger.Error("DB Pastes list error", err)
		http.Error(w, "DB error", http.StatusInternalServerError)
		return
	}

	result := make([]models.Paste, 0)
	for _, p := range pastes {
		pasteModel := models.Paste{
			ID:     p.ID,
			UserID: p.UserID,
			Name:   p.Name,
			Token:  p.Token,
			Text:   p.Text,
		}
		result = append(result, pasteModel)
	}
	data["pastes"] = result

	pastesFiles := []string{"./static/templates/pastes.page.tmpl", files[1]}

	err = tempate.ExecuteTemplate(r.Context(), w, pastesFiles)
	if err != nil {
		h.Logger.Error("Template error", err)
		http.Error(w, "Template error", http.StatusInternalServerError)
		return
	}
}

func (h *PasteHandler) PasteIt(w http.ResponseWriter, r *http.Request) {
	err := tempate.ExecuteTemplate(r.Context(), w, files)
	if err != nil {
		h.Logger.Error("Template error", err)
		http.Error(w, "Template error", http.StatusInternalServerError)
		return
	}
}

func (h *PasteHandler) Paste(w http.ResponseWriter, r *http.Request) {
	pasteName := r.FormValue("name")
	pasteText := r.FormValue("text")

	data := contextData.GetDataFromContext(r.Context())

	userID := data["userID"].(int)
	id, err := h.PasteRepo.GetPastesCount()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	pasteID := id + 1

	pasteToken := token.GenerateToken(pasteID, viper.GetString("pasteit.salt"))


	p := &models.Paste{
		UserID: userID,
		Name:   pasteName,
		Token:  pasteToken,
		Text:   pasteText,
	}

	_, err = h.PasteRepo.Create(p)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	http.Redirect(w, r, "/pasteit/"+ p.Token, 302)
}
