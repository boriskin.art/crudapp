package repository

import (
	"crudapp/internal/pkg/models"
	"database/sql"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"testing"
)

func TestPostgresPasteRepo_Create(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)

	defer db.Close()

	repo := &PostgresPasteRepo{DB: db}

	testPaste := &models.Paste{
		UserID: 0,
		Name:   "Test",
		Token:  "123456",
		Text:   "Test",
	}

	// good request
	mock.ExpectExec("INSERT INTO").
		WithArgs(testPaste.UserID, testPaste.Token, testPaste.Name, testPaste.Text).
		WillReturnResult(sqlmock.NewResult(1, 1))

	id, err := repo.Create(testPaste)

	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Equal(t, 1, int(id))

	// bad request
	mock.ExpectExec("INSERT INTO").
		WithArgs(testPaste.UserID, testPaste.Token, testPaste.Name, testPaste.Text).
		WillReturnError(fmt.Errorf("db_error"))

	id, err = repo.Create(testPaste)

	assert.Error(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Equal(t, 0, int(id))
}

func TestPostgresPasteRepo_GetPastesCount(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)

	defer db.Close()

	repo := &PostgresPasteRepo{DB: db}

	rows := sqlmock.NewRows([]string{"count"}).AddRow(1)


	// good request
	mock.ExpectQuery("SELECT").
		WillReturnRows(rows)

	count, err := repo.GetPastesCount()

	assert.Equal(t, 1, count)
	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())

	// db_error
	mock.ExpectQuery("SELECT").
		WillReturnError(fmt.Errorf("db_error"))

	count, err = repo.GetPastesCount()

	assert.Equal(t, 0, count)
	assert.Error(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestPostgresPasteRepo_GetByToken(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)

	defer db.Close()

	repo := &PostgresPasteRepo{DB: db}

	var token = "123456"

	expect := &models.Paste{
		ID:     1,
		UserID: 1,
		Name:   "Test",
		Token:  token,
		Text:   "Test",
	}

	rows := sqlmock.NewRows([]string{"id", "user_id", "token", "name", "text"}).
		AddRow(expect.ID, expect.UserID, expect.Token, expect.Name, expect.Text)

	// good request
	mock.ExpectQuery("SELECT").
		WithArgs(token).
		WillReturnRows(rows)

	p, err := repo.GetByToken(token)

	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Equal(t, expect, p)

	// bad request
	mock.ExpectQuery("SELECT").
		WithArgs(token).
		WillReturnError(fmt.Errorf("db_error"))

	p, err = repo.GetByToken(token)

	assert.Error(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Nil(t, p)

	// no pastes
	mock.ExpectQuery("SELECT").
		WithArgs(token).
		WillReturnError(sql.ErrNoRows)

	p, err = repo.GetByToken(token)

	assert.Error(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Nil(t, p)

}

func TestPostgresPasteRepo_GetByUserID(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)

	defer db.Close()

	repo := &PostgresPasteRepo{DB: db}

	var userID = 1

	expect := []models.Paste{
		{
			ID:     1,
			UserID: userID,
			Name:   "Test",
			Token:  "123456",
			Text:   "Test",
		},
	}

	rows := sqlmock.NewRows([]string{"id", "user_id", "token", "name", "text"}).
		AddRow(expect[0].ID, expect[0].UserID, expect[0].Token, expect[0].Name, expect[0].Text)

	// good request
	mock.ExpectQuery("SELECT").
		WithArgs(userID).
		WillReturnRows(rows)

	p, err := repo.GetByUserID(userID)

	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Equal(t, expect, p)

	// bad request
	mock.ExpectQuery("SELECT").
		WithArgs(userID).
		WillReturnError(fmt.Errorf("db_error"))

	p, err = repo.GetByUserID(userID)

	assert.Error(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Nil(t, p)

	// no pastes
	mock.ExpectQuery("SELECT").
		WithArgs(userID).
		WillReturnError(sql.ErrNoRows)

	p, err = repo.GetByUserID(userID)

	assert.Error(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Nil(t, p)

	// row scan error
	rows = sqlmock.NewRows([]string{"id"}).AddRow(1)

	mock.ExpectQuery("SELECT").
		WithArgs(userID).
		WillReturnRows(rows)

	p, err = repo.GetByUserID(userID)

	assert.Error(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Nil(t, p)
}