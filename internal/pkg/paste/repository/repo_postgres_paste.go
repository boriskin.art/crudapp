package repository

import (
	"crudapp/internal/pkg/models"
	"database/sql"
)

type PostgresPasteRepo struct {
	DB *sql.DB
}

func NewPostgresPasteRepo(db *sql.DB) *PostgresPasteRepo {
	return &PostgresPasteRepo{
		DB: db,
	}
}

func (repo *PostgresPasteRepo) GetPastesCount() (int, error) {
	var id int
	err := repo.DB.QueryRow(`SELECT COUNT(*) FROM paste`).Scan(&id)
	if err != nil {
		return 0, err
	}

	return id, nil
}


func (repo *PostgresPasteRepo) Create(p *models.Paste) (int64, error){
	result, err := repo.DB.Exec(`INSERT INTO paste (user_id, token, name, text) VALUES ($1, $2, $3, $4)`,
		p.UserID, p.Token, p.Name, p.Text)

	if err != nil {
		return 0, err
	}

	return result.RowsAffected()
}

func (repo *PostgresPasteRepo) GetByUserID(userID int) ([]models.Paste, error) {
	pastes := make([]models.Paste, 0)

	rows, err := repo.DB.Query(`SELECT id, user_id, token, name, text FROM paste WHERE user_id=$1`, userID)
	if err == sql.ErrNoRows {
		return nil, models.ErrNoPaste
	}
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		paste := models.Paste{}
		err := rows.Scan(&paste.ID, &paste.UserID, &paste.Token, &paste.Name, &paste.Text)
		if err != nil {
			return nil, err
		}

		pastes = append(pastes, paste)
	}

	return pastes, nil
}

func (repo *PostgresPasteRepo) GetByToken(token string) (*models.Paste, error) {
	paste := &models.Paste{}

	err := repo.DB.QueryRow(`SELECT id, user_id, token, name, text FROM paste WHERE token=$1`, token).
		Scan(&paste.ID, &paste.UserID, &paste.Token, &paste.Name, &paste.Text)
	if err == sql.ErrNoRows {
		return nil, models.ErrNoPaste
	}
	if err != nil {
		return nil, err
	}

	return paste, nil
}