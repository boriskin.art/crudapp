package repository

import (
	"crudapp/internal/pkg/models"
	"database/sql"
)

// Postgres repository for links
type PostgresLinkRepo struct {
	DB *sql.DB
}

// Init repo
func NewPostgresLinkRepo(db *sql.DB) *PostgresLinkRepo {
	return &PostgresLinkRepo{DB: db}
}

// Get link by token
func (repo *PostgresLinkRepo) GetByToken(token string) (*models.Link, error) {
	link := &models.Link{}

	err := repo.DB.QueryRow(`SELECT id, token, long_link, clicks, user_id FROM link WHERE token=$1`, token).
		Scan(&link.ID, &link.Token, &link.LongLink, &link.ClicksCount, &link.UserID)
	if err == sql.ErrNoRows {
		return nil, models.ErrNoLink
	}
	if err != nil {
		return nil, err
	}

	return link, nil
}

// Get link by long link
func (repo *PostgresLinkRepo) GetByLongLinkAndUserID(longLink string, userID int) (*models.Link, error) {
	link := &models.Link{}

	err := repo.DB.QueryRow(`SELECT id, token, long_link, clicks, user_id FROM link WHERE long_link=$1 and user_id=$2`, longLink, userID).
		Scan(&link.ID, &link.Token, &link.LongLink, &link.ClicksCount, &link.UserID)
	if err == sql.ErrNoRows {
		return nil, models.ErrNoLink
	}
	if err != nil {
		return nil, err
	}

	return link, nil
}

// Get links slice by userID
func (repo *PostgresLinkRepo) GetByUserID(userID int) ([]*models.Link, error) {
	links := make([]*models.Link, 0)

	rows, err := repo.DB.Query(`SELECT id, token, long_link, clicks, user_id FROM link WHERE user_id=$1`, userID)
	if err == sql.ErrNoRows {
		return nil, models.ErrNoLink
	}
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	for rows.Next() {
		link := &models.Link{}
		err := rows.Scan(&link.ID, &link.Token, &link.LongLink, &link.ClicksCount, &link.UserID)
		if err != nil {
			return nil, err
		}

		links = append(links, link)
	}
	return links, nil
}

func (repo *PostgresLinkRepo) GetCount() (int, error) {
	var id = 0
	err := repo.DB.QueryRow(`SELECT COUNT(*) FROM link`).Scan(&id)
	if err != nil {
		return 0, err
	}
	return id, nil
}

// Create new link from long link and userID, returned link model
func (repo *PostgresLinkRepo) Create(l *models.Link) (int64, error) {
	_, err := repo.DB.Exec(
		`INSERT INTO link ("token", "long_link", "clicks", "user_id") VALUES ($1, $2, $3, $4)`,
		l.Token,
		l.LongLink,
		0,
		l.UserID,
	)
	if err != nil {
		return 0, err
	}

	return 1, nil
}

// Increment clicks counter for link by token
func (repo *PostgresLinkRepo) Click(token string) (int64, error) {
	_, err := repo.DB.Exec(`UPDATE link SET clicks = clicks + 1 WHERE token = $1;`, token)
	if err != nil {
		return 0, err
	}
	return 1, nil
}