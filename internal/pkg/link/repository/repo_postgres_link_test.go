package repository

import (
	"crudapp/internal/pkg/models"
	"database/sql"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"testing"
)

func TestPostgresLinkRepo_GetByUserID(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)

	defer db.Close()

	repo := PostgresLinkRepo{DB: db}

	var userID = 1
	rows := sqlmock.NewRows([]string{"id", "token", "long_link", "clicks", "user_id"})
	expect := []*models.Link{
		{ID: 1, Token: "123456", LongLink: "https://test.ru", ClicksCount: 0, UserID: userID},
		{ID: 2, Token: "654321", LongLink: "http://test.com", ClicksCount: 0, UserID: userID},
	}

	for _, l := range expect {
		rows.AddRow(l.ID, l.Token, l.LongLink, l.ClicksCount, l.UserID)
	}

	// good query
	mock.ExpectQuery("SELECT").
		WithArgs(userID).
		WillReturnRows(rows)

	links, err := repo.GetByUserID(userID)

	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())

	for i, l := range links {
		assert.Equal(t, expect[i], l)
	}

	// no links for user
	mock.ExpectQuery("SELECT").
		WithArgs(userID).
		WillReturnError(sql.ErrNoRows)

	links, err = repo.GetByUserID(userID)

	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Error(t, err)


	// bad request
	mock.ExpectQuery("SELECT").
		WithArgs(userID).
		WillReturnError(fmt.Errorf("db_error"))

	links, err = repo.GetByUserID(userID)

	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Error(t, err)

	// rows scan error
	rows = sqlmock.NewRows([]string{"id", "token"}).
		AddRow(1, "123456")

	mock.ExpectQuery("SELECT").
		WithArgs(userID).
		WillReturnRows(rows)

	links, err = repo.GetByUserID(userID)

	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Error(t, err)
}


func TestPostgresLinkRepo_GetByLongLinkAndUserID(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)

	defer db.Close()

	repo := PostgresLinkRepo{DB: db}

	var longLink = "http://yandex.ru/"
	var userID = 1
	rows := sqlmock.NewRows([]string{"id", "token", "long_link", "clicks", "user_id"})
	expect := &models.Link{ID: 1, Token: "123456", LongLink: longLink, ClicksCount: 0, UserID: userID}


	rows.AddRow(expect.ID, expect.Token, expect.LongLink, expect.ClicksCount, expect.UserID)


	// good query
	mock.ExpectQuery("SELECT").
		WithArgs(longLink, userID).
		WillReturnRows(rows)

	links, err := repo.GetByLongLinkAndUserID(longLink, userID)

	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())

	assert.Equal(t, expect, links)

	// no links for user
	mock.ExpectQuery("SELECT").
		WithArgs(longLink, userID).
		WillReturnError(sql.ErrNoRows)

	links, err = repo.GetByLongLinkAndUserID(longLink, userID)

	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Error(t, err)
	assert.Nil(t, links)


	// bad request
	mock.ExpectQuery("SELECT").
		WithArgs(longLink, userID).
		WillReturnError(fmt.Errorf("db_error"))

	links, err = repo.GetByLongLinkAndUserID(longLink, userID)

	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Error(t, err)
	assert.Nil(t, links)
}

func TestPostgresLinkRepo_GetByToken(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)

	defer db.Close()

	repo := PostgresLinkRepo{DB: db}

	var token = "123456"
	rows := sqlmock.NewRows([]string{"id", "token", "long_link", "clicks", "user_id"})
	expect := &models.Link{ID: 1, Token: token, LongLink: "http://yandex.ru/", ClicksCount: 0, UserID: 1}


	rows.AddRow(expect.ID, expect.Token, expect.LongLink, expect.ClicksCount, expect.UserID)


	// good query
	mock.ExpectQuery("SELECT").
		WithArgs(token).
		WillReturnRows(rows)

	links, err := repo.GetByToken(token)

	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())

	assert.Equal(t, expect, links)

	// no links for user
	mock.ExpectQuery("SELECT").
		WithArgs(token).
		WillReturnError(sql.ErrNoRows)

	links, err = repo.GetByToken(token)

	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Error(t, err)
	assert.Nil(t, links)


	// bad request
	mock.ExpectQuery("SELECT").
		WithArgs(token).
		WillReturnError(fmt.Errorf("db_error"))

	links, err = repo.GetByToken(token)

	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Error(t, err)
	assert.Nil(t, links)
}

func TestPostgresLinkRepo_Click(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)

	defer db.Close()

	repo := &PostgresLinkRepo{DB: db}

	var token = "123456"

	// good query
	mock.ExpectExec("UPDATE").WithArgs(token).WillReturnResult(sqlmock.NewResult(1, 1))

	id, err := repo.Click(token)

	assert.Equal(t, 1, int(id))
	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())

	// bad query
	mock.ExpectExec("UPDATE").WithArgs(token).WillReturnError(fmt.Errorf("db_error"))

	id, err = repo.Click(token)

	assert.Equal(t, 0, int(id))
	assert.Error(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestPostgresLinkRepo_Create(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)

	defer db.Close()

	repo := &PostgresLinkRepo{DB: db}

	testLink := models.Link{
		ID:          1,
		Token:       "123456",
		LongLink:    "yandex.ru",
		ClicksCount: 0,
		UserID:      1,
	}

	// good request
	mock.ExpectExec("INSERT").
		WithArgs(testLink.Token, testLink.LongLink, testLink.ClicksCount, testLink.UserID).
		WillReturnResult(sqlmock.NewResult(1, 1))

	id, err := repo.Create(&testLink)

	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Equal(t, 1, int(id))


	// bad request
	mock.ExpectExec("INSERT").
		WithArgs(testLink.Token, testLink.LongLink, testLink.ClicksCount, testLink.UserID).
		WillReturnError(fmt.Errorf("db_error"))

	id, err = repo.Create(&testLink)

	assert.Error(t, err)
	assert.Equal(t, 0, int(id))
	assert.NoError(t, mock.ExpectationsWereMet())
}


func TestPostgresLinkRepo_GetCount(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)

	defer db.Close()

	repo := PostgresLinkRepo{DB: db}

	rows := sqlmock.NewRows([]string{"count"})

	rows.AddRow(1)

	// good query
	mock.ExpectQuery("SELECT").
		WillReturnRows(rows)

	id, err := repo.GetCount()

	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Equal(t, 1, id)

	// db_error
	mock.ExpectQuery("SELECT").
		WillReturnError(fmt.Errorf("db_error"))

	id, err = repo.GetCount()

	assert.Error(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Equal(t, 0, id)
}
