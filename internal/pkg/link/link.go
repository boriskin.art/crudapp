package link

import "crudapp/internal/pkg/models"

//go:generate mockgen -destination=./mock/mock_repo.go -package=mock crudapp/internal/pkg/link Repository

// Links repository interface
type Repository interface {
	GetByToken(token string) (*models.Link, error)
	GetByLongLinkAndUserID(longLink string, userID int) (*models.Link, error)
	GetByUserID(userID int) ([]*models.Link, error)
	Create(l *models.Link) (int64, error)
	Click(token string) (int64, error)
	GetCount() (int, error)
}