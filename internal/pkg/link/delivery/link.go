package delivery

import (
	"crudapp/internal/crudapp/tempate"
	"crudapp/internal/pkg/link"
	"crudapp/internal/pkg/models"
	"crudapp/pkg/contextData"
	"crudapp/pkg/token"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/spf13/viper"
	"go.uber.org/zap"
	"html/template"
	"net/http"
	"os"
	"time"
)

type LinkHandler struct {
	LinkRepo 	link.Repository
	Tmpl 		*template.Template
	Logger		*zap.SugaredLogger
}

var files = []string{
"./static/templates/shortit.page.tmpl",
"./static/templates/base.layout.tmpl",
}

// Links list page "/shortit/links"
func (h *LinkHandler) Links(w http.ResponseWriter, r *http.Request) {
	// Get data from context
	data := contextData.GetDataFromContext(r.Context())

	// Get userID from data
	userID := data["userID"].(int)

	// Get all links, that have same userID
	links, err := h.LinkRepo.GetByUserID(userID)
	if err != nil {
		h.Logger.Error("DB Links list error", err)
		http.Error(w, "DB error", http.StatusInternalServerError)
		return
	}

	// Make result slice from response
	result := make([]models.Link, 0)
	for _, l := range links {
		pasteModel := models.Link{
			ID:     	 l.ID,
			Token:  	 l.Token,
			LongLink: 	 l.LongLink,
			ClicksCount: l.ClicksCount,
			UserID: 	 l.UserID,

		}
		result = append(result, pasteModel)
	}
	// Save data to context
	data["links"] = result

	// Change template path
	pastesFiles := []string{"./static/templates/links.page.tmpl", files[1]}

	err = tempate.ExecuteTemplate(r.Context(), w, pastesFiles)
	if err != nil {
		h.Logger.Error("Template error", err)
		http.Error(w, "Template error", http.StatusInternalServerError)
		return
	}
}

// Main page of ShortIt!
func (h *LinkHandler) ShortIt(w http.ResponseWriter, r *http.Request) {
	err := tempate.ExecuteTemplate(r.Context(), w, files)
	if err != nil {
		h.Logger.Error("Template error", err)
		http.Error(w, "Template error", http.StatusInternalServerError)
		return
	}
}

// Handler of new link
func (h *LinkHandler) Short(w http.ResponseWriter, r *http.Request) {
	// Get long link from request
	longLink := r.FormValue("link")

	data := contextData.GetDataFromContext(r.Context())
	userID := data["userID"].(int)

	// Check link, need to http[s]://...
	longLink = models.MakeLink(longLink)

	l, err := h.LinkRepo.GetByLongLinkAndUserID(longLink, userID)

	// Create new record in db
	if err != nil {
		count, err := h.LinkRepo.GetCount()
		t := token.GenerateToken(count + 1, viper.GetString("shortit.salt"))
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		l = &models.Link{
			ID: 		 count + 1,
			Token:       t,
			LongLink:    longLink,
			ClicksCount: 0,
			UserID:      userID,
		}
		_, err = h.LinkRepo.Create(l)
		if err != nil {
			fmt.Println(err)
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
	// Form short link from domain and token
	shortedLink := viper.GetString("domain") + "/shortit/" + l.Token

	// Get Qr for short link
	QRImg, err := models.GenerateQRCode(shortedLink, l.Token)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Goroutine, that delete qr img from tmp in 5 min
	go func(QRImg string) {
		time.Sleep(5 * time.Minute)
		os.Remove(QRImg)
	}(QRImg)

	// Save data to context
	data["QRImg"] = QRImg
	data["Link"] = shortedLink
	r.WithContext(contextData.SaveDataToContext(data))

	// Display the template
	err = tempate.ExecuteTemplate(r.Context(), w, files)
	if err != nil {
		h.Logger.Error("Template error", err)
		http.Error(w, "Template error", http.StatusInternalServerError)
		return
	}
}

// Open long link from token
func (h *LinkHandler) Open(w http.ResponseWriter, r *http.Request) {
	t := mux.Vars(r)["token"]

	l, err := h.LinkRepo.GetByToken(t)
	fmt.Println("1", l, err)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Increment counter of clicks in db
	_, err = h.LinkRepo.Click(t)
	fmt.Println("2", err)
	if err != nil {
		h.Logger.Error("DB error", err)
		http.Error(w, "DB error", http.StatusInternalServerError)
	}

	// Redirect to long link
	http.Redirect(w, r, l.LongLink, 302)
}