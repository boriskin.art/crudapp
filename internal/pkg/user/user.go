package user

import "crudapp/internal/pkg/models"

//go:generate mockgen -destination=./mock/mock_repo.go -package=mock crudapp/internal/pkg/user Repository

// User repository interface
type Repository interface {
	GetByID(id int) (*models.User, error)
	GetByLogin(login string) (*models.User, error)
	GetList(limit, offset int) ([]*models.User, error)
	Create(u *models.User) (int, error)
	Authorize(login, password string) (*models.User, error)
}