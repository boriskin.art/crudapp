package repository

import (
	"crudapp/internal/pkg/models"
	"database/sql"
)


type PostgresUserRepo struct {
	DB *sql.DB
}


func NewPostgresUserRepo(db *sql.DB) *PostgresUserRepo {
	return &PostgresUserRepo{DB: db}
}

// Метод поиска в репозитории пользователя по переданному логину
// Возвращает пользователя, ошибку если произошла
func (repo *PostgresUserRepo) GetByID(id int) (*models.User, error) {
	user := &models.User{}

	err := repo.DB.
		QueryRow(`SELECT id, login, password FROM users WHERE id = $1`, id).
		Scan(&user.ID, &user.Login, &user.Password)

	if err != nil {
		return nil, err
	}

	return user, nil
}

func (repo *PostgresUserRepo) GetByLogin(login string) (*models.User, error) {
	user := &models.User{}

	err := repo.DB.
		QueryRow(`SELECT id, login, password FROM users WHERE login = $1`, login).
		Scan(&user.ID, &user.Login, &user.Password)

	if err == sql.ErrNoRows {
		return nil, models.ErrNoUser
	}
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (repo *PostgresUserRepo) GetList(limit, offset int) ([]*models.User, error) {
	users := make([]*models.User, 0, limit)

	rows, err := repo.DB.Query("SELECT id, login, password FROM users OFFSET $1 LIMIT $2", offset, limit)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		user := &models.User{}
		err := rows.Scan(&user.ID, &user.Login, &user.Password)
		if err != nil {
			return nil, err
		}
		users = append(users, user)
	}
	return users, nil
}

func (repo *PostgresUserRepo) Create(u *models.User) (int, error) {
	var id int
	err := repo.DB.QueryRow(
		`INSERT INTO users ("login", "password") VALUES ($1, $2) RETURNING id`,
		u.Login,
		u.Password,
	).Scan(&id)

	if err != nil {
		return 0, err
	}

	return id, nil
}

// Метод проверки пароля пользователя
// Если пользователь найден и прошел проверку пароля, отправляем его
func (repo *PostgresUserRepo) Authorize(login, password string) (*models.User, error) {
	u, err := repo.GetByLogin(login)
	if err != nil {
		return nil, models.ErrNoUser
	}

	if models.CheckPass(u.Password, password) {
		return u, nil
	}

	return nil, err
}