package repository

import (
	"crudapp/internal/pkg/models"
	"database/sql"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"testing"
)

func TestPostgresUserRepo_GetByID(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)
	defer db.Close()

	repo := &PostgresUserRepo{
		DB: db,
	}

	var userID = 1

	// good query
	rows := sqlmock.NewRows([]string{"id", "login", "password"})

	expected := []*models.User{
		{ID: uint64(userID), Login: "example@example.ru", Password: "qwerty"},
	}

	for _, user := range expected {
		rows.AddRow(user.ID, user.Login, user.Password)
	}

	mock.ExpectQuery("SELECT id, login, password").
		WithArgs(userID).
		WillReturnRows(rows)

	user, err := repo.GetByID(userID)
	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Equal(t, expected[0], user)

	// query error
	mock.ExpectQuery("SELECT id, login, password FROM users WHERE").
		WithArgs(userID).
		WillReturnError(fmt.Errorf("db_error"))

	_, err = repo.GetByID(userID)
	assert.Error(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestPostgresUserRepo_GetByLogin(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)
	defer db.Close()

	repo := &PostgresUserRepo{
		DB: db,
	}

	var userLogin = "test@test.ru"

	// good query
	rows := sqlmock.NewRows([]string{"1", "test@test.ru", "qwerty"})

	expect := []*models.User{
		{ID: 1, Login: userLogin, Password: "qwerty"},
	}

	for _, user := range expect {
		rows.AddRow(user.ID, user.Login, user.Password)
	}

	mock.ExpectQuery("SELECT id, login, password FROM users WHERE").
		WithArgs(userLogin).
		WillReturnRows(rows)

	user, err := repo.GetByLogin(userLogin)
	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())

	assert.Equal(t, expect[0], user)

	// bad request
	mock.ExpectQuery("SELECT id, login, password FROM users WHERE").
		WithArgs("no user").
		WillReturnError(sql.ErrNoRows)

	_, err = repo.GetByLogin("no user")
	assert.Error(t, err)


	// query error
	mock.ExpectQuery("SELECT id, login, password FROM users WHERE").
		WithArgs(userLogin).
		WillReturnError(fmt.Errorf("db error"))

	_, err = repo.GetByLogin(userLogin)
	assert.Error(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
}

func TestPostgresUserRepo_GetList(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)
	defer db.Close()

	repo := &PostgresUserRepo{DB: db}

 	// good query
 	rows := sqlmock.NewRows([]string{"id", "login", "password"})

 	expect := []*models.User{
 		{ID: 1, Login: "test1", Password: "qwerty"},
 		{ID: 2, Login: "test2", Password: "qwerty"},
	}

	for _, user := range expect {
		rows.AddRow(user.ID, user.Login, user.Password)
	}

	mock.ExpectQuery("SELECT id, login, password FROM users").WillReturnRows(rows)

	users, err := repo.GetList(10, 0)

	assert.NoError(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())

	for i, u := range users {
		assert.Equal(t, expect[i], u)
	}

	// rows scan error
	rows = sqlmock.NewRows([]string{"id", "login"}).
		AddRow(1, "test")

	mock.
		ExpectQuery("SELECT id, login, password").
		WillReturnRows(rows)

	users, err = repo.GetList(10, 0)
	assert.Error(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())

	// query error
	mock.
		ExpectQuery("SELECT id, login, password FROM users").
		WillReturnError(fmt.Errorf("db_error"))

	_, err = repo.GetList(10, 0)
	assert.Error(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())



}

func TestPostgresUserRepo_Create(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)

	defer db.Close()

	repo := &PostgresUserRepo{DB: db}

	testUser := &models.User{
		Login:    "user1",
		Password: "pass",
	}

	rows := sqlmock.NewRows([]string{"id"}).AddRow(1)

	// good query
	mock.
		ExpectQuery("INSERT INTO users").
		WithArgs(testUser.Login, testUser.Password).
		WillReturnRows(rows)

	id, err := repo.Create(testUser)

	assert.NoError(t, mock.ExpectationsWereMet())
	assert.NoError(t, err)
	assert.Equal(t, 1, id)

	// query error
	mock.
		ExpectQuery("INSERT INTO users").
		WithArgs(testUser.Login, testUser.Password).
		WillReturnError(fmt.Errorf("bad request"))

	id, err = repo.Create(testUser)
	assert.Error(t, err)
	assert.NoError(t, mock.ExpectationsWereMet())
	assert.Equal(t, 0, id)

}