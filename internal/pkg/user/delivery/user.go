package delivery

import (
	"crudapp/internal/crudapp/tempate"
	"crudapp/internal/pkg/models"
	"crudapp/internal/pkg/session"
	"crudapp/internal/pkg/user"
	"crypto/rand"
	"fmt"
	"go.uber.org/zap"
	"html/template"
	"net/http"
	"strconv"
)

type UserHandler struct {
	UserRepo 	user.Repository
	Tmpl 		*template.Template
	Logger		*zap.SugaredLogger
	SessionRepo session.Repository
}

func (h *UserHandler) Index(w http.ResponseWriter, r *http.Request) {
	files := []string{
		"./static/templates/index.page.tmpl",
		"./static/templates/base.layout.tmpl",
	}
	err := tempate.ExecuteTemplate(r.Context(), w, files)
	if err != nil {
		h.Logger.Error("Template error", err)
		http.Error(w, "Template error", http.StatusInternalServerError)
		return
	}
}

func (h *UserHandler) About(w http.ResponseWriter, r *http.Request) {
	files := []string{
		"./static/templates/about.page.tmpl",
		"./static/templates/base.layout.tmpl",
	}
	err := tempate.ExecuteTemplate(r.Context(), w, files)
	if err != nil {
		h.Logger.Error("Template error", err)
		http.Error(w, "Template error", http.StatusInternalServerError)
		return
	}
}

func (h *UserHandler) Users(w http.ResponseWriter, _ *http.Request) {
	users, err := h.UserRepo.GetList(10, 0)
	if err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
	}
	fmt.Fprint(w, users)
}

func (h *UserHandler) SignUp(w http.ResponseWriter, r *http.Request) {

	files := []string{
		"./static/templates/signup.page.tmpl",
		"./static/templates/base.layout.tmpl",
	}
	err := tempate.ExecuteTemplate(r.Context(), w, files)
	if err != nil {
		h.Logger.Error("Template error", err)
		http.Error(w, "Template error", http.StatusInternalServerError)
		return
	}
}

func (h *UserHandler) SignUpPost(w http.ResponseWriter, r *http.Request) {
	login := r.FormValue("login")
	password := r.FormValue("password")


	if _, err := h.UserRepo.GetByLogin(login); err != models.ErrNoUser {
		http.Redirect(w, r, "/signup", http.StatusFound)
		return
	}

	salt := make([]byte, 8)
	rand.Read(salt)

	hashedPass := models.PasswordPBKDF2(fmt.Sprintf("%x", salt), password)

	u := &models.User{
		Login: login,
		Password: hashedPass,
	}

	id, err := h.UserRepo.Create(u)
	if err != nil {
		h.Logger.Error(err)
	}
	http.Redirect(w, r, "/session?user_id=" + strconv.Itoa(int(id)), http.StatusFound)
}

func (h *UserHandler) SignIn(w http.ResponseWriter, r *http.Request) {
	files := []string{
		"./static/templates/signin.page.tmpl",
		"./static/templates/base.layout.tmpl",
	}

	err := tempate.ExecuteTemplate(r.Context(), w, files)
	if err != nil {
		h.Logger.Error("Template error", err)
		http.Error(w, "Template error", http.StatusInternalServerError)
		return
	}
}

func (h *UserHandler) SignInPost(w http.ResponseWriter, r *http.Request) {
	login := r.FormValue("login")
	password := r.FormValue("password")
	u, err := h.UserRepo.Authorize(login, password)

	if err == models.ErrNoUser {
		http.Redirect(w, r, "/signin", http.StatusFound)
		return
	}

	if err != nil {
		h.Logger.Error(err)
	}

	http.Redirect(w, r, "/session?user_id=" + strconv.Itoa(int(u.ID)), http.StatusFound)
}

func (h *UserHandler) GetUserByID(id int) (*models.User, error) {
	return h.UserRepo.GetByID(id)
}

func (h *UserHandler) GetUserByLogin(login string) (*models.User, error) {
	return h.UserRepo.GetByLogin(login)
}

