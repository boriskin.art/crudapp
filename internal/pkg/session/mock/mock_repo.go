// Code generated by MockGen. DO NOT EDIT.
// Source: crudapp/internal/pkg/session (interfaces: Repository)

// Package mock is a generated GoMock package.
package mock

import (
	models "crudapp/internal/pkg/models"
	reflect "reflect"

	gomock "github.com/golang/mock/gomock"
)

// MockRepository is a mock of Repository interface.
type MockRepository struct {
	ctrl     *gomock.Controller
	recorder *MockRepositoryMockRecorder
}

// MockRepositoryMockRecorder is the mock recorder for MockRepository.
type MockRepositoryMockRecorder struct {
	mock *MockRepository
}

// NewMockRepository creates a new mock instance.
func NewMockRepository(ctrl *gomock.Controller) *MockRepository {
	mock := &MockRepository{ctrl: ctrl}
	mock.recorder = &MockRepositoryMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockRepository) EXPECT() *MockRepositoryMockRecorder {
	return m.recorder
}

// CheckSession mocks base method.
func (m *MockRepository) CheckSession(arg0 string) (*models.Session, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "CheckSession", arg0)
	ret0, _ := ret[0].(*models.Session)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// CheckSession indicates an expected call of CheckSession.
func (mr *MockRepositoryMockRecorder) CheckSession(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CheckSession", reflect.TypeOf((*MockRepository)(nil).CheckSession), arg0)
}

// CloseSession mocks base method.
func (m *MockRepository) CloseSession(arg0 int) error {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "CloseSession", arg0)
	ret0, _ := ret[0].(error)
	return ret0
}

// CloseSession indicates an expected call of CloseSession.
func (mr *MockRepositoryMockRecorder) CloseSession(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CloseSession", reflect.TypeOf((*MockRepository)(nil).CloseSession), arg0)
}

// CreateSession mocks base method.
func (m *MockRepository) CreateSession(arg0 *models.Session) (int64, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "CreateSession", arg0)
	ret0, _ := ret[0].(int64)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// CreateSession indicates an expected call of CreateSession.
func (mr *MockRepositoryMockRecorder) CreateSession(arg0 interface{}) *gomock.Call {
	mr.mock.ctrl.T.Helper()
	return mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "CreateSession", reflect.TypeOf((*MockRepository)(nil).CreateSession), arg0)
}
