package delivery

import (
	"crudapp/internal/pkg/models"
	"crudapp/internal/pkg/session"
	"crudapp/pkg/contextData"
	"github.com/spf13/viper"
	"go.uber.org/zap"
	"net/http"
	"strconv"
	"time"
)


type SessionHandler struct {
	SessionRepo  session.Repository
	Logger		 *zap.SugaredLogger
}

func (h *SessionHandler) Logout(w http.ResponseWriter, r *http.Request) {
	data := contextData.GetDataFromContext(r.Context())
	err := h.SessionRepo.CloseSession(data["userID"].(int))

	data["userID"] = 0
	r.WithContext(contextData.SaveDataToContext(data))

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	cookie := http.Cookie{
		Name:    "session_id",
		Expires: time.Now().AddDate(0, 0, -1),
		Domain:    "." + viper.GetString("domain"),
	}
	http.SetCookie(w, &cookie)

	http.Redirect(w, r, "/", http.StatusFound)
}


func (h *SessionHandler) Session(w http.ResponseWriter, r *http.Request) {
	userID, err := strconv.Atoi(r.FormValue("user_id"))
	if err != nil {
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	s := &models.Session{
		UserID:  userID,
		Token:   models.GenerateSessionToken(userID),
		Created: time.Now(),
		Expires: time.Now().Add(7 * 24 * time.Hour),
	}


	_, err = h.SessionRepo.CreateSession(s)
	if err != nil {
		http.Redirect(w, r, "/signin", http.StatusInternalServerError)
		return
	}

	cookie := &http.Cookie{
		Domain: "." + viper.GetString("domain"),
		Name: "session_id",
		Value: s.Token,
		Expires: s.Expires,
	}

	http.SetCookie(w, cookie)

	http.Redirect(w, r, "/", http.StatusFound)
}