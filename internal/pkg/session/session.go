package session

import (
	"crudapp/internal/pkg/models"
)

//go:generate mockgen -destination=./mock/mock_repo.go -package=mock crudapp/internal/pkg/session Repository

type Repository interface {
	CreateSession(s *models.Session) (int64, error)
	CheckSession(token string) (*models.Session, error)
	CloseSession(userID int) error
}
