package repository

import (
	"crudapp/internal/pkg/models"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
	"testing"
	"time"
)

func TestPostgresSessionRepo_CreateSession(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)

	defer db.Close()

	repo := &PostgresSessionRepo{DB: db}

	testSession := &models.Session{
		ID:      1,
		UserID:  1,
		Token:   "test",
		Created: time.Time{},
		Expires: time.Time{}.Add(24 * time.Hour),
	}

	// good query
	mock.ExpectExec("INSERT INTO session").
		WithArgs(testSession.UserID, testSession.Token, testSession.Created, testSession.Expires).
		WillReturnResult(sqlmock.NewResult(1, 1))

	id, err := repo.CreateSession(testSession)
	assert.NoError(t, err)
	assert.Equal(t, 1, int(id))


	// bad query
	mock.ExpectExec("INSERT INTO session").
		WithArgs(testSession.UserID, testSession.Token, testSession.Created, testSession.Expires).
		WillReturnError(fmt.Errorf("bad query"))

	id, err = repo.CreateSession(testSession)
	assert.Error(t, err)
	assert.Equal(t, int64(0), id)
}

func TestPostgresSessionRepo_CheckSession(t *testing.T) {
	db, mock, err := sqlmock.New()
	assert.NoError(t, err)

	defer db.Close()

	repo := &PostgresSessionRepo{DB: db}

	var testToken = "test"

	expect := []*models.Session{
		{ID: 1, UserID: 1, Token: testToken, Created: time.Now(), Expires: time.Now().Add(24 * time.Hour)},
	}

	rows := sqlmock.NewRows([]string{"id", "user_id", "token", "created", "expires"}).
		AddRow(expect[0].ID, expect[0].UserID, expect[0].Token, expect[0].Created, expect[0].Expires)

	mock.ExpectQuery("SELECT id, user_id, token, created, expires FROM session").
		WillReturnRows(rows)

	s, err := repo.CheckSession(testToken)

	assert.NoError(t, err)
	assert.Equal(t, expect[0], s)
	assert.NoError(t, mock.ExpectationsWereMet())


	// bad request
	mock.ExpectQuery("SELECT id, user_id, token, created, expires FROM session").
		WillReturnError(fmt.Errorf("bad request"))

	s, err = repo.CheckSession(testToken)

	assert.Error(t, err)
	assert.Nil(t, s)
	assert.NoError(t, mock.ExpectationsWereMet())

}