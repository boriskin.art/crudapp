package repository

import (
	"crudapp/internal/pkg/models"
	"database/sql"
	"time"
)


type PostgresSessionRepo struct {
	DB *sql.DB
}


func NewPostgresSessionRepo(db *sql.DB) *PostgresSessionRepo {
	return &PostgresSessionRepo{DB: db}
}


func (repo *PostgresSessionRepo) CreateSession(s *models.Session) (int64, error) {
	result, err := repo.DB.Exec(
		`INSERT INTO session ("user_id", "token", "created", "expires") VALUES ($1, $2, $3, $4)`,
		s.UserID,
		s.Token,
		s.Created,
		s.Expires,
	)

	if err != nil {
		return 0, err
	}

	return result.RowsAffected()
}


func (repo *PostgresSessionRepo) CheckSession(token string) (*models.Session, error) {
	session := &models.Session{}

	err := repo.DB.QueryRow(`SELECT id, user_id, token, created, expires FROM session WHERE token=$1`, token).
		Scan(&session.ID, &session.UserID, &session.Token, &session.Created, &session.Expires)

	if err != nil {
		return nil, err
	}

	if session.Expires.Before(time.Now()) {
		return nil, models.ErrExpiredSession
	}

	if token != session.Token {
		return nil, models.ErrNoSession
	}

	return session, nil
}

func (repo *PostgresSessionRepo) CloseSession(userID int) error {
	_, err := repo.DB.Exec(`DELETE FROM session WHERE user_id=$1`, userID)
	if err != nil {
		return err
	}

	return nil
}
