package models

import (
	"errors"
	"github.com/speps/go-hashids/v2"
	"time"
)

// Main session struct
type Session struct {
	ID 		   int
	UserID	   int
	Token 	   string
	Created    time.Time
	Expires	   time.Time
}

// Errors
var (
	ErrNoSession = errors.New("the session was not found")
	ErrExpiredSession = errors.New("session was expired")
)

// Generate token for new session
func GenerateSessionToken(userID int) string {
	hd := hashids.NewData()
	hd.Salt = "crudapp_sessions"
	hd.MinLength = 32
	h, _ := hashids.NewWithData(hd)

	token, _ := h.Encode([]int{userID})
	return token
}
