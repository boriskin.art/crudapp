package models

import (
	"crypto/sha1"
	"errors"
	"fmt"
	"golang.org/x/crypto/pbkdf2"
)

// Main User struct
type User struct {
	ID			uint64
	Login 		string
	Password	string
}

// Errors
var (
	ErrNoUser	= errors.New("no user found")
)

// Hashing the password
func PasswordPBKDF2(salt, plainPassword string) string {
	hashedPass := pbkdf2.Key([]byte(plainPassword), []byte(salt), 4096, 32, sha1.New)
	return salt + fmt.Sprintf("%x", hashedPass)
}

// Check password from user
func CheckPass(passHash string, plainPassword string) bool {
	salt := passHash[0:16]
	userPassHash := PasswordPBKDF2(fmt.Sprintf("%s", salt), plainPassword)
	return userPassHash == passHash
}
