package models

import "errors"

// Main Paste struct
type Paste struct {
	ID 		int
	UserID  int
	Name 	string
	Token 	string
	Text 	string
}

// Errors
var (
	ErrNoPaste = errors.New("no pastes")
)