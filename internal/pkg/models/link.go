package models

import (
	"errors"
	"github.com/yeqown/go-qrcode"
	"strings"
)

// Main link struct
type Link struct {
	ID			int
	Token 		string
	LongLink 	string
	ClicksCount int
	UserID 		int
}

// Errors
var (
	ErrNoLink = errors.New("the link was not found")
)

// Link must be prefixed with "http..."
func CheckLink(link string) bool {
	return strings.HasPrefix(strings.TrimSpace(link), "http")
}

// Check link for prefix and add it, if not
func MakeLink(data string) string {
	if !CheckLink(data) {
		return "http://" + data
	}
	return data
}

// Generate qr code for link and save it to tmp dir
func GenerateQRCode(link, token string) (string, error) {
	qrc, err := qrcode.New(link)
	if err != nil {
		return "", err
	}

	QRpath := "static/tmp/" + token + ".jpeg"
	if err := qrc.Save("./" + QRpath); err != nil {
		return "", err
	}

	return QRpath, nil
}