package databases_connections

import (
	"database/sql"
	"log"
)

// Connect to postgres
func GetPostgres(dsn string) *sql.DB {
	db, err := sql.Open("pgx", dsn)
	if err != nil {
		log.Fatalln("cant parse postgres config", err)
	}

	// Check connection
	err = db.Ping()
	if err != nil {
		log.Fatalln(err)
	}

	db.SetMaxOpenConns(50)

	return db
}
