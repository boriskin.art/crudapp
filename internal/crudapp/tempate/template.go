package tempate

import (
	"context"
	contextData "crudapp/pkg/contextData"
	"html/template"
	"net/http"
)

// Execute template to page with data from context
func ExecuteTemplate(ctx context.Context, w http.ResponseWriter, files []string) error {
	ts, err := template.ParseFiles(files...)
	if err != nil {
		return err
	}

	data := contextData.GetDataFromContext(ctx)

	err = ts.Execute(w, data)
	if err != nil {
		return err
	}

	return nil
}
