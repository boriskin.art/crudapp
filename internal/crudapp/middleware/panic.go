package middleware

import (
	"log"
	"net/http"
)

// Panic middleware. Need to recover panics, which can occur during query execution
func Panic(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		// Just add recover defer
		defer func() {
			if err := recover(); err != nil {
				log.Println(err)
				http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			}
		}()

		next.ServeHTTP(w, r)
	})
}