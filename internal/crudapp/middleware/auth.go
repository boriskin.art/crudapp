package middleware

import (
	"context"
	"crudapp/internal/pkg/session/repository"
	"fmt"
	"net/http"
	"strings"
)

// Hosts that don't require authorization
var (
	withoutSession = map[string]struct{}{
		"/signin": {},
		"/signup": {},
		"/session": {},
		"/logout": {},
		"/static": {},
		"/about": {},
	}
)

// Auth middleware, checks the user's permissions to view the content
func Auth(s *repository.PostgresSessionRepo, next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		// Check the presence of a session or the presence of a host in the list, which does not require authorization
		cookie, err := r.Cookie("session_id")
		var sessionID = ""
		if err == nil {
			sessionID = cookie.Value
		}
		session, err := s.CheckSession(sessionID)
		_, ok := withoutSession[r.URL.Path]
		if (!ok && err != nil) && !strings.HasPrefix(r.URL.Path, "/static") {
			fmt.Println(ok, err)
			http.Redirect(w, r, "/signin", http.StatusFound)
			return
		}

		// If user can access page without auth or user authorized save id (or 0) to context
		userID := 0
		if session != nil {
			userID = session.UserID

		}

		// Pass the request on, putting the context inside
		ctx := context.WithValue(r.Context(), "user_id", userID)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}