package middleware

import (
	"context"
	"crudapp/internal/pkg/models"
	"github.com/spf13/viper"
	"net/http"
	"strings"
)

// ContextMiddleware save all needed data to context
func ContextData(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		data := map[string]interface{}{
			"service": strings.Split(r.URL.Path, "/")[1],
			"domain": viper.GetString("domain"),
			"userID": r.Context().Value("user_id").(int),
			"QRImg": "",
			"Link": "",
			"Paste": "",
			"pasteName": "",
			"pasteText": "",
			"pastes": []models.Paste{},
			"links": []models.Link{},
		}

		// Pass the request on, putting the context inside
		ctx := context.WithValue(r.Context(), "data", data)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}
