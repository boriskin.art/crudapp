package middleware

import (
	"go.uber.org/zap"
	"net/http"
	"time"
)

// Access log middleware
// Outputs every request to service into logger (ZapLogger)
func AccessLog(logger *zap.SugaredLogger, next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// fmt.Println("Access log middleware")
		start := time.Now()
		next.ServeHTTP(w, r)
		logger.Infow("New request",
			"method", r.Method,
			"remote_addr", r.RemoteAddr,
			"host", r.Host,
			"url", r.URL.Path,
			"time", time.Since(start),
		)
	})
}