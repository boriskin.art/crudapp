package token

import "github.com/speps/go-hashids/v2"

// Generate 7-digit token for links and pastes
func GenerateToken(id int, salt string) string {
	hd := hashids.NewData()
	hd.Salt = salt
	hd.MinLength = 7
	h, _ := hashids.NewWithData(hd)

	token, _ := h.Encode([]int{id})
	return token
}
