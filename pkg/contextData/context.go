package contextData

import "context"

// Returns data from context, which we put in contextData middleware
func GetDataFromContext(ctx context.Context) map[string]interface{} {
	return ctx.Value("data").(map[string]interface{})
}

// Save data to context
func SaveDataToContext(data map[string]interface{}) context.Context {
	return context.WithValue(context.Background(), "data", data)
}