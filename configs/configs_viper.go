package configs

import (
	"github.com/spf13/viper"
	_ "github.com/spf13/viper/remote"
	"log"
	"os"
)


// Read config file and get passwords from env
func ParseConfigs() {
	viper.AddConfigPath("./configs")
	viper.SetConfigName("config")

	if err := viper.ReadInConfig(); err != nil {
		log.Fatal(err)
	}

	viper.Set("db.users_db.password", os.Getenv("DB_PASSWORD"))
	viper.Set("db.sessions_db.password", os.Getenv("DB_PASSWORD"))
	viper.Set("db.links_db.password", os.Getenv("DB_PASSWORD"))
	viper.Set("db.pastes_db.password", os.Getenv("DB_PASSWORD"))
}
