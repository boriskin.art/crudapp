package main

import (
	config "crudapp/configs"
	db "crudapp/internal/crudapp/databases_connections"
	"crudapp/internal/crudapp/middleware"
	linkDelivery "crudapp/internal/pkg/link/delivery"
	linkRepo "crudapp/internal/pkg/link/repository"
	pasteDelivery "crudapp/internal/pkg/paste/delivery"
	pasteRepo "crudapp/internal/pkg/paste/repository"
	"crudapp/internal/pkg/session/delivery"
	sessionRepo "crudapp/internal/pkg/session/repository"
	userDelivery "crudapp/internal/pkg/user/delivery"
	userRepo "crudapp/internal/pkg/user/repository"
	"flag"
	"fmt"
	"github.com/gorilla/mux"
	_ "github.com/jackc/pgx/stdlib"
	"github.com/spf13/viper"
	"go.uber.org/zap"
	"html/template"
	"net/http"
)

// Main func
func main() {
	// Init zapLogger
	zapLogger, _ := zap.NewProduction()
	defer zapLogger.Sync()
	logger := zapLogger.Sugar()

	// Templates directory
	templates := template.Must(template.ParseGlob("./static/templates/*"))

	config.ParseConfigs()

	var addr string
	var domain string

	flag.StringVar(&addr, "addr", viper.GetString("port"), "Change server addr")
	flag.StringVar(&domain, "domain", viper.GetString("domain"), "Change site domain")
	flag.Parse()

	viper.Set("port", addr)
	viper.Set("domain", domain)

	//dsnUsers := fmt.Sprintf(`host=localhost port=5432 dbname=crudapp user=artem sslmode=disable`)
	dsnUsers := fmt.Sprintf(`host=%v port=%v dbname=%v user=%v password=%v sslmode=%v sslrootcert=%v`,
		viper.GetString("db.users_db.host"),
		viper.GetString("db.users_db.port"),
		viper.GetString("db.users_db.dbname"),
		viper.GetString("db.users_db.user"),
		viper.GetString("db.users_db.password"),
		viper.GetString("db.users_db.sslmode"),
		viper.GetString("db.users_db.sslrootcert"))

	//dsnLinks := fmt.Sprintf(`host=localhost port=5432 dbname=crudapp user=artem sslmode=disable`)
	dsnSession := fmt.Sprintf(`host=%v port=%v dbname=%v user=%v password=%v sslmode=%v sslrootcert=%v`,
		viper.GetString("db.sessions_db.host"),
		viper.GetString("db.sessions_db.port"),
		viper.GetString("db.sessions_db.dbname"),
		viper.GetString("db.sessions_db.user"),
		viper.GetString("db.sessions_db.password"),
		viper.GetString("db.sessions_db.sslmode"),
		viper.GetString("db.sessions_db.sslrootcert"))


	dsnLinks := fmt.Sprintf(`host=%v port=%v dbname=%v user=%v password=%v sslmode=%v sslrootcert=%v`,
		viper.GetString("db.links_db.host"),
		viper.GetString("db.links_db.port"),
		viper.GetString("db.links_db.dbname"),
		viper.GetString("db.links_db.user"),
		viper.GetString("db.links_db.password"),
		viper.GetString("db.links_db.sslmode"),
		viper.GetString("db.links_db.sslrootcert"))

	dsnPaste := fmt.Sprintf(`host=%v port=%v dbname=%v user=%v password=%v sslmode=%v sslrootcert=%v`,
		viper.GetString("db.pastes_db.host"),
		viper.GetString("db.pastes_db.port"),
		viper.GetString("db.pastes_db.dbname"),
		viper.GetString("db.pastes_db.user"),
		viper.GetString("db.pastes_db.password"),
		viper.GetString("db.pastes_db.sslmode"),
		viper.GetString("db.pastes_db.sslrootcert"))


	// Session DB repository
	sessionRepository := sessionRepo.NewPostgresSessionRepo(db.GetPostgres(dsnSession))
	sessionHandler := &delivery.SessionHandler{
		SessionRepo: sessionRepository,
		Logger:      logger,
	}

	// Users DB repository
	userRepository := userRepo.NewPostgresUserRepo(db.GetPostgres(dsnUsers))
	userHandler := &userDelivery.UserHandler{
		Tmpl: templates,
		UserRepo: userRepository,
		Logger: logger,
		SessionRepo: sessionRepository,
	}

	// Links DB repository
	linkRepository := linkRepo.NewPostgresLinkRepo(db.GetPostgres(dsnLinks))
	linkHandler := &linkDelivery.LinkHandler{
		LinkRepo: linkRepository,
		Tmpl:     templates,
		Logger:   logger,
	}

	// pastes DB repository
	pasteRepository := pasteRepo.NewPostgresPasteRepo(db.GetPostgres(dsnPaste))
	pasteHandler := &pasteDelivery.PasteHandler{
		PasteRepo: pasteRepository,
		Tmpl:     templates,
		Logger:   logger,
	}

	router := mux.NewRouter()

	// ShortIt!
	router.HandleFunc("/shortit/links", linkHandler.Links).Methods("GET")
	router.HandleFunc("/shortit/{token}", linkHandler.Open).Methods("GET")
	router.HandleFunc("/shortit", linkHandler.ShortIt).Methods("GET")
	router.HandleFunc("/shortit", linkHandler.Short).Methods("POST")

	// PasteIt!
	router.HandleFunc("/pasteit/pastes", pasteHandler.Pastes)
	router.HandleFunc("/pasteit/{token}", pasteHandler.Open).Methods("GET")
	router.HandleFunc("/pasteit", pasteHandler.PasteIt).Methods("GET")
	router.HandleFunc("/pasteit", pasteHandler.Paste).Methods("POST")

	// Main pages handlers
	router.HandleFunc("/", userHandler.Index).Methods("GET")
	router.HandleFunc("/about", userHandler.About).Methods("GET")

	// Auth handler
	router.HandleFunc("/users", userHandler.Users).Methods("GET")
	router.HandleFunc("/signup", userHandler.SignUp).Methods("GET")
	router.HandleFunc("/signup", userHandler.SignUpPost).Methods("POST")
	router.HandleFunc("/signin", userHandler.SignIn).Methods("GET")
	router.HandleFunc("/signin", userHandler.SignInPost).Methods("POST")

	// Session handlers
	router.HandleFunc("/session", sessionHandler.Session).Methods("GET")
	router.HandleFunc("/logout", sessionHandler.Logout).Methods("GET")

	// Static files
	router.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("./static"))))

	// Middlewares
	m := middleware.ContextData(router)
	m = middleware.Auth(sessionRepository, m)
	m = middleware.AccessLog(logger, m)
	m = middleware.Panic(m)


	// Starting server
	logger.Infow("starting server", "type", "START", "addr", "http://" + viper.GetString("domain") + viper.GetString("port"))
	logger.Fatal(http.ListenAndServe(viper.GetString("port"), m))
}